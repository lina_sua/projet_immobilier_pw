<?php
class Favoris{
  private $pseudo;	
  private $id_bienImmobilier;
  
  public function __construct($pseudo, $id_bienImmobilier) {
	$this->pseudo = $pseudo;
	$this->id_bienImmobilier = $id_bienImmobilier;
  }
  public function getPseudo() {
     return $this->pseudo;
  }
    public function getBienImmobilier() {
     return $this->id_bienImmobilier;
  }
  
  public function setBienImmobilier($t) {
       $this->id_bienImmobilier = $t;
  }
    
  public function __toString() {
	return $this->pseudo.",".$this->id_bienImmobilier;
  }
}

?>