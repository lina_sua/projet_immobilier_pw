<?php
class Compte{
  private $pseudo;	
  private $mdp;


  public function __construct($p,$m) {
	$this->pseudo= $p;
    $this->mdp = $m;
  }
  
    public function getPseudo() {
     return $this->pseudo;
  }
  
  public function setPseudo($p) {
       $this->pseudo = $p;
  }

  public function getMdp() {
     return $this->mdp;
  }
  
  
  public function setMdp($m) {
       $this->mdp = $m;
  }
  
  
  public function __toString() {
	return $this->pseudo.",".$this->mdp;
  }
}

?>