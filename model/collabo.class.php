<?php
class Collaborator{
  private $nom;	
  private $poste;
  private $image;
  
  
  public function __construct() {
	$this->nom= "";
    $this->poste = "";
    $this->image = "";
  }
  
    public function getNom() {
     return $this->nom;
  }
  
  public function setNom($t) {
       $this->nom = $t;
  }
  public function getPoste() {
     return $this->poste;
  }
  
  public function setposte($t) {
       $this->poste = $t;
  }

  public function getImage() {
     return $this->image;
  }
  
  public function setImage($i) {
       $this->image = $i;
  }
  
}

?>