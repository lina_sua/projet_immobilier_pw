<?php
class Profil{
  private $nom;	
  private $prenom;
  private $email;
  private $statut;
  private $date_creation;
  private $pseudo;

  public function __construct($nom,$prenom, $email,$pseudo) {
	$this->nom = $nom;
    $this->prenom = $prenom;
    $this->email = $email;
	$this->pseudo = $pseudo;
  }
  
    public function getNom() {
     return $this->nom;
  }
  
  public function setNom($nom) {
       $this->nom = $nom;
  }
  public function getPrenom() {
     return $this->prenom;
  }
  
  public function setPrenom($p) {
       $this->prenom = $p;
  }

  public function getEmail() {
     return $this->email;
  }
  
  
  public function setEmail($email) {
       $this->email = $email;
  }
  public function getStatut() {
     return $this->statut;
  }
  
    public function setStatut($s) {
       $this->statut= $s;
  }
  
    public function getDate_creation() {
     return $this->date_creation;
  }
  
  public function getPseudo() {
     return $this->pseudo;
  }
  
  public function __toString() {
	return $this->nom.",".$this->prenom.",".$this->email.",".$this->statut.$this->date_creation. $this->pseudo;
  }
}

?>