<?php
class Biens{
  private $titre;	
  private $type;
  private $prix;
  private $ville;
  private $codePostal;
  private $adresse;
  private $description;
  private $pseudo;
  
  public function __construct($titre,$type, $prix,$ville, $codePostal, $adresse, $description, $pseudo) {
	$this->titre = $titre;
    $this->type = $type;
    $this->prix = $prix;
	$this->ville = $ville;
	$this->codePostal = $codePostal;
	$this->adresse = $adresse;
	$this->description = $description;
	$this->pseudo = $pseudo;
  }
  
    public function getTitre() {
     return $this->titre;
  }
  
  public function setTitre($t) {
       $this->titre = $t;
  }
  public function getType1() {
     return $this->type;
  }
  
  public function setType1($p) {
       $this->type = $p;
  }

  public function getPrix() {
     return $this->prix;
  }
  
  public function setPrix($prix) {
       $this->prix = $prix;
  }
  public function getVille() {
     return $this->ville;
  }
  public function getCodePostal() {
     return $this->codePostal;
  }
  public function getAdresse() {
     return $this->adresse;
  }
  
   public function getDescription() {
     return $this->description;
  }
  
  public function getPseudo() {
     return $this->pseudo;
  }
  
  public function __toString() {
	return $this->titre.",".$this->type.",".$this->prix.",".$this->ville.",".$this->codePostal.",". $this->adresse.",".$this->description ;
  }
}

?>