<?php

session_start();
if(!isset($_SESSION['login'])) //verification pour le pseudo
{
 //Si la session n'est pas ouverte, redirection vers la page du formulaire
header("Location:../views/connection.php");
exit();
}
require_once("../util/config.php");
include("../model/compte.class.php");
include("../dao/daoProfil.php");
include("../model/profil.class.php");
include("../dao/dapCompt.php");
	
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title></title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Law Firm Website Template" name="keywords">
        <meta content="Law Firm Website Template" name="description">

        <!-- Favicon -->
        <link href="img/favicon.ico" rel="icon">

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=EB+Garamond:ital,wght@1,600;1,700;1,800&family=Roboto:wght@400;500&display=swap" rel="stylesheet"> 
        
        <!-- CSS Libraries -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
        <link href="lib/animate/animate.min.css" rel="stylesheet">
        <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

        <!-- Template Stylesheet -->
        <link href="../css/style.css" rel="stylesheet">
    </head>

    <body>
        <div class="wrapper">
            <!-- Top Bar Start -->
            <div class="top-bar">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="logo">
                             </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="top-bar-right">
                                <div class="text">
                                    <h2></h2>
                                    <p></p>
                                </div>
                                <div class="text">
                                </div>
                                <div class="social">
                                    <a href=""><i class="fab fa-twitter"></i></a>
                                    <a href=""><i class="fab fa-facebook-f"></i></a>
                                    <a href=""><i class="fab fa-linkedin-in"></i></a>
                                    <a href=""><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->
           
 <!-- About Start -->
            <div class="about">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-5 col-md-6">
                            <div class="about-img">
                                <img src="../img/img2.jfif" alt="Image">
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-6">
				<h2>partie compte utilisateur</h2>
					<p> Bonjour,
												!
						<?php
						//creation des variables pseudo=id, mot de passe = mdp, confirmation mot de passe=confmdp
						$id=htmlspecialchars(addslashes($_POST['pseudo']));
						$mdp=htmlspecialchars(addslashes($_POST['mdp']));
						$confmdp = htmlspecialchars(addslashes($_POST['confmdp']));
						?>
						
						<?php
						$confirmation = strcmp ( $mdp, $confmdp );// aide a compararer le mot de passe avec sa confirmation
						if ($confirmation== 0)//si la comparation de mot passe et confirmation est vrai, on continu
						{//confirmation de mots passe
						

						$comp= new Compte($id, $mdp);
						$uncompte= new DAOCompte($comp);
						$uncompte->add();
// creations de variables pour nom, prenom et email
								$nom1 =htmlspecialchars(addslashes($_POST["nom"])); // variables qui sont recuperé
								$prenom1=htmlspecialchars(addslashes($_POST["prenom"]));
								$email1 = htmlspecialchars(addslashes($_POST["email"]));
								//creation d'un profil lorsque la compte est crée
								$prof= new Profil($nom1, $prenom1, $email1, $id);
								$uncompte= new DAOProfil($prof);
								$uncompte->add();	
echo "<div class='navbar-nav mr-auto'>
                                <a href='../views/connection.php' class='nav-item nav-link active'>connectez vous</a>
			    </div>   ";
						
							
								?>
								
								<?php
								
								}else {
							echo "les mots passe et la confirmation de mot passe ne sont pas identiques" . "\n";
							
							
						}
						?> 
						
						 
								
								
						
                                </p>
                                <p>
                                </p>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- About End -->
        <!-- JavaScript Libraries -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
        <script src="lib/easing/easing.min.js"></script>
        <script src="lib/owlcarousel/owl.carousel.min.js"></script>
        <script src="lib/isotope/isotope.pkgd.min.js"></script>

        <!-- Template Javascript -->
        <script src="../js/main.js"></script>
    </body>
</html>
