<?php
session_start();
if(!isset($_SESSION['login'])) //verification pour le pseudo
{
 //Si la session n'est pas ouverte, redirection vers la page du formulaire
header("Location:../views/connection.php");
exit();
}

?>

<?php //destruction de la session
session_destroy();
// libération des variables globales associées à la session
unset($_SESSION['login']);
unset($_SESSION['statut']);
//redirection vers l'accueil
header("Location:../index.php");
?>
