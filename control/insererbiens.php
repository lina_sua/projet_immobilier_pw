<?php
session_start();
if(!isset($_SESSION['login'])) //verification pour le pseudo
{
 //Si la session n'est pas ouverte, redirection vers la page du formulaire
header("Location:../views/connection.php");
exit();
}
if($_SESSION['statut'] ==  'A'){

require_once("../util/config.php");
include("../model/biens.class.php");
include("../dao/daoBiens.php");
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title></title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Law Firm Website Template" name="keywords">
        <meta content="Law Firm Website Template" name="description">

        <!-- Favicon -->
        <link href="../img/favicon.ico" rel="icon">

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=EB+Garamond:ital,wght@1,600;1,700;1,800&family=Roboto:wght@400;500&display=swap" rel="stylesheet"> 
        
        <!-- CSS Libraries -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
        <link href="lib/animate/animate.min.css" rel="stylesheet">
        <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

        <!-- Template Stylesheet -->
        <link href="../css/style.css" rel="stylesheet">
    </head>

    <body>
        <div class="wrapper">
            <!-- Top Bar Start -->
            <div class="top-bar">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="logo">
                                                 </div>
                        </div>
                       

                        <div class="col-lg-9">
                            <div class="top-bar-right">
                                <div class="text">
                                    <h2></h2>
                                    <p></p>
                                </div>
                                <div class="text">
                                    
					
                                </div>
                                <div class="social">
                                    <a href=""><i class="fab fa-twitter"></i></a>
                                    <a href=""><i class="fab fa-facebook-f"></i></a>
                                    <a href=""><i class="fab fa-linkedin-in"></i></a>
                                    <a href=""><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->
           
 <!-- About Start -->
            <div class="about">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-5 col-md-6">
                            <div class="about-img">
                                <img src="../img/img2.jfif" alt="Image">
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-6">
				<h2>partie bien  utilisateur</h2>
					
					 
						 
								
								
						
                                <p>
				
				
					<?php
					
	
	$option=$_POST['option1'];
	if($option == 'Inserter'){
        $titre=htmlspecialchars(addslashes($_POST['titre']));
        $type=htmlspecialchars(addslashes($_POST['type']));
        $prix=htmlspecialchars(addslashes($_POST['prix']));
        $ville=htmlspecialchars(addslashes($_POST['ville']));
        $codePostal=htmlspecialchars(addslashes($_POST['codePostal']));
        $adresse=htmlspecialchars(addslashes($_POST['adresse']));
        $description=htmlspecialchars(addslashes($_POST['description']));
        
        $biens1= new Biens($titre, $type,$prix,$ville,$codePostal,$adresse,$description,$_SESSION['login'] );
        $unBiens= new DAOBiens($biens1);
        $unBiens->add();
        echo("il a était inserer"); 
	}
	if($option == 'Modifier'){
        $titre=htmlspecialchars(addslashes($_POST['titre']));
        $type=htmlspecialchars(addslashes($_POST['type']));
        $prix=htmlspecialchars(addslashes($_POST['prix']));
        $ville=htmlspecialchars(addslashes($_POST['ville']));
        $codePostal=htmlspecialchars(addslashes($_POST['codePostal']));
        $adresse=htmlspecialchars(addslashes($_POST['adresse']));
        $description=htmlspecialchars(addslashes($_POST['description']));
        
        $biens1= new Biens($titre, $type,$prix,$ville,$codePostal,$adresse,$description,$_SESSION['login'] );
        $unBiens= new DAOBiens($biens1);
        $unBiens->update();
        echo("il a était modifier"); 
	}
	if($option == 'Eliminer'){
        $titre=htmlspecialchars(addslashes($_POST['titre']));
        $type=htmlspecialchars(addslashes($_POST['type']));
        
        
        $host = 'localhost';
        $dbname = 'lina';
        $username = 'root';
        $password = '';
        $dsn ="mysql:host=$host;dbname=$dbname";
        $sql = " delete from t_bienimmobilier where immo_titre= '".$titre."' and immo_type= '".$type. "'; ";

        try{
            
            $connection = new PDO($dsn, $username, $password);
            $req = $connection->query($sql);
            echo("il a était éliminé"); 
        }catch (PDOException $e){
            print "Erreur !: " . $e->getMessage() . "<br/>";
            die();
        }
        
    }
	}

					?>
		
<a href="../views/admin_accueil.php" class="nav-item nav-link active">accueil compte</a>

                                </p>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- About End -->
        <!-- JavaScript Libraries -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
        <script src="lib/easing/easing.min.js"></script>
        <script src="lib/owlcarousel/owl.carousel.min.js"></script>
        <script src="lib/isotope/isotope.pkgd.min.js"></script>

        <!-- Template Javascript -->
        <script src="../js/main.js"></script>
    </body>
</html>
