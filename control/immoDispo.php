
<?php
/* Vérification ci-dessous à faire sur toutes les pages dont l'accès est
autorisé à un utilisateur connecté. */
session_start();
if(!isset($_SESSION['login'])) //verification pour le pseudo
{
 //Si la session n'est pas ouverte, redirection vers la page du formulaire
header("Location:../views/connection.php");
exit();
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title></title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Law Firm Website Template" name="keywords">
        <meta content="Law Firm Website Template" name="description">

        <!-- Favicon -->
        <link href="../img/favicon.ico" rel="icon">

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=EB+Garamond:ital,wght@1,600;1,700;1,800&family=Roboto:wght@400;500&display=swap" rel="stylesheet"> 
        
        <!-- CSS Libraries -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
        <link href="lib/animate/animate.min.css" rel="stylesheet">
        <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

        <!-- Template Stylesheet -->
        <link href="../css/style.css" rel="stylesheet">
    </head>

    <body>
        <div class="wrapper">
            <!-- Top Bar Start -->
            <div class="top-bar">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="logo">
							
                                                 </div>
                        </div>
                       

                        <div class="col-lg-9">
                            <div class="top-bar-right">
                                <div class="text">
								
                                    <h2></h2>
                                    <p></p>
                                </div>
                                <div class="text">
                                    
					
                                </div>
                                <div class="social">
                                    <a href=""><i class="fab fa-twitter"></i></a>
                                    <a href=""><i class="fab fa-facebook-f"></i></a>
                                    <a href=""><i class="fab fa-linkedin-in"></i></a>
                                    <a href=""><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->
			<!-- Nav Bar Start -->
            <div class="nav-bar">
                <div class="container-fluid">
                    <nav class="navbar navbar-expand-lg bg-dark navbar-dark">
                        <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                            <div class="navbar-nav mr-auto">
				<a href="../index.php" class="nav-item nav-link active">home</a>
                                <a href="../views/admin_accueil.php" class="nav-item nav-link active">accueil compte</a>

								<a href="favorites.php" class="nav-item nav-link">aller à favorites</a>
								<a href="desconexion.php" class="nav-item nav-link">Déconnexion</a>
				
			    </div>    
                        </div>
                    </nav>
                </div>
            </div>
            <!-- Nav Bar End -->
           
 <!-- About Start -->
            <div class="about">
                <div class="container">
                    <div class="row align-items-center">
                        
                        <div class="col-lg-7 col-md-6">
				<h2>biens disponibles </h2>
                                <p>
								
				<?php
				//appelle base des données
					$host = 'localhost';
	$dbname = 'lina';
	$username = 'root';
	$password = '';
	
	$dsn ="mysql:host=$host;dbname=$dbname";
			
	
	$sql = "SELECT * FROM t_bienimmobilier;";
	
	try{
		$connection = new PDO($dsn, $username, $password);
		$req = $connection->query($sql);
		echo "<br>";
							//tableau des selections
							echo("<table class = 'table-bordered'>"); 
								echo("<tr>");
								echo("<th>"); echo("titre"); echo("</th>");
								echo("<th>"); echo("typte"); echo("</th>");
								echo("<th>"); echo("prix"); echo("</th>");
								echo("<th>"); echo("ville"); echo("</th>");
								echo("<th>"); echo("codePostal"); echo("</th>");
								echo("<th>"); echo("adresse"); echo("</th>");
								echo("<th>"); echo("desccription"); echo("</th>");
								echo("<th>"); echo("date d'ajoute"); echo("</th>");
								echo("<th>"); echo("image presentation"); echo("</th>");
								echo("<th>"); echo(""); echo("</th>");
								echo("</tr>");
								while ($immo = $req->fetch(PDO::FETCH_ASSOC))
							{	
		//Affichage de la requête préparée
			
						
						echo("<td>"); echo($immo['immo_titre']);echo("</td>");
						echo("<td>"); echo($immo['immo_type']);echo("</td>");
						echo("<td>"); echo($immo['immo_prix']);echo("</td>");
						echo("<td>"); echo($immo['immo_ville']);echo("</td>");
						echo("<td>"); echo($immo['immo_codePostal']);echo("</td>");
						echo("<td>"); echo($immo['immo_adresse']);echo("</td>");
						echo("<td>"); echo($immo['immo_description']);echo("</td>");
						echo("<td>"); echo($immo['immo_date_ajoute']);echo("</td>");
						$sql1= "SELECT nom_image, id_image from t_image where id_image in(select id_image from t_banderole join t_bienimmobilier using(id_bienImmobilier) where id_bienImmobilier = ".$immo['id_bienImmobilier'].") ;";
						$req1 = $connection->query($sql1);
						$bandImag = $req1->fetch(PDO::FETCH_ASSOC);
						echo("<td>");echo ("<img width='500' src='../img/". $bandImag['nom_image'] ."'>");
						echo("<td>"); echo("<div class='about-img'> <img src='../img/".$bandImag['nom_image']."' alt='Image'> </div> ");echo("</td>");
						echo("<td>");echo('<a href="affichage_imag.php?sid1='.$immo['id_bienImmobilier'].'&id_i='.$bandImag['id_image'].' "class="nav-item nav-link active">voir</a>'); echo("</td>");
						
						echo("<tr>");
						
						
							}
							
							echo("</table");
	}catch(PDOException $e){
		echo ("Erreur entrer base données : ".$e->getMessage());
	}
		
			
				
					?>
				
				
		


                                </p>
                                <p>
								
                                </p>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- About End -->
        <!-- JavaScript Libraries -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
        <script src="lib/easing/easing.min.js"></script>
        <script src="lib/owlcarousel/owl.carousel.min.js"></script>
        <script src="lib/isotope/isotope.pkgd.min.js"></script>

        <!-- Template Javascript -->
        <script src="../js/main.js"></script>
    </body>
</html>