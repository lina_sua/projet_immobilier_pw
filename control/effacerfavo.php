<?php
session_start();
if(!isset($_SESSION['login'])) //verification pour le pseudo
{
 //Si la session n'est pas ouverte, redirection vers la page du formulaire
header("Location:../views/connection.php");
exit();
}
require_once("../util/config.php");
include("../model/favoris.class.php");
include("../dao/daoFavoris.php");

$pseudo= $_GET['id_i1'];
$bienImo= $_GET['sid2'];

$prof= new Favoris($pseudo,$bienImo);
$unfavo= new DAOFavoris($prof);
$unfavo->delete1();

header("Location:favorites.php");
exit();
?>