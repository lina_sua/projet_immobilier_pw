
<?php
session_start();

//creation des variables pseudo, mot de passe = mdp
	$pseudo=htmlspecialchars(addslashes($_POST['pseudo']));
	$mdp=htmlspecialchars(addslashes($_POST['mdp'])); 

?>


<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <title></title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Law Firm Website Template" name="keywords">
        <meta content="Law Firm Website Template" name="description">

        <!-- Favicon -->
        <link href="../img/favicon.ico" rel="icon">

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=EB+Garamond:ital,wght@1,600;1,700;1,800&family=Roboto:wght@400;500&display=swap" rel="stylesheet"> 
        
        <!-- CSS Libraries -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
        <link href="lib/animate/animate.min.css" rel="stylesheet">
        <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

        <!-- Template Stylesheet -->
        <link href="../css/style.css" rel="stylesheet">
    </head>

    <body>
        <div class="wrapper">
            <!-- Top Bar Start -->
            <div class="top-bar">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="logo">
                                <a href="../index.php">
                                    <h1>retourner sur home</h1>
                                    <!-- <img src="img/logo.jpg" alt="Logo"> -->
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="top-bar-right">
                                
                                <div class="text">
                                    <h2>select_action</h2>
                                    <p></p>
                                </div>
                                <div class="social">
                                    <a href=""><i class="fab fa-twitter"></i></a>
                                    <a href=""><i class="fab fa-facebook-f"></i></a>
                                    <a href=""><i class="fab fa-linkedin-in"></i></a>
                                    <a href=""><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->

            
            <!-- About Start -->
            <div class="about">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-5 col-md-6">
                            <div class="about-img">
                                <img src="../img/img2.jfif" alt="Image">
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-6">
				
                                <p>				
					<h2>partie session</h2>
					<p> 
						<?php
	$host = 'localhost';
	$dbname = 'lina';
	$username = 'root';
	$password = '';
	
	$dsn ="mysql:host=$host;dbname=$dbname";
	
	$sql="SELECT pseudo, mot_passe, statut  FROM t_compte_utilisateur JOIN t_profil_utilisateur USING(pseudo) WHERE pseudo= '".$pseudo."' AND mot_passe=MD5('".$mdp."') ;";
	
        try{
            $connection = new PDO($dsn, $username, $password);
            $req = $connection->query($sql);
            $rows = $req->rowCount();
            if($rows==1){
            //Mise à jour des données de la session
            //variable pour recouperer le pseudo de la session
            $_SESSION['login']=$pseudo;
            //asociacion de la requette sql à la variable statut, pour recuperer le statut de l'utilisateur
            while($statut=$req->fetch(PDO::FETCH_ASSOC)){
                //recuperation du statut
            $_SESSION['statut']= $statut['statut'];
            echo("esteeeeeee".$_SESSION['statut']);
            //redirection vers admin_accueil.php
            header("Location:../views/admin_accueil.php");
            }
			
		}else{
									// aucune ligne retournée
									 // => le compte n'existe pas ou n'est pas valide
									echo "pseudo/mot de passe incorrect(s) ou profil inconnu !";
									echo "<br /><a href=\"../views/connection.php\">Cliquez ici pour réafficher
									 le formulaire</a>";
									 
								}
		
	}catch(PDOException $e){
		echo ("Erreur entrer base données : ".$e->getMessage());
	}

							
						?>
						

                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- About End -->
        

            <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
        </div>

        <!-- JavaScript Libraries -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
        <script src="lib/easing/easing.min.js"></script>
        <script src="lib/owlcarousel/owl.carousel.min.js"></script>
        <script src="lib/isotope/isotope.pkgd.min.js"></script>

        <!-- Template Javascript -->
        <script src="js/main.js"></script>
    </body>
</html>
