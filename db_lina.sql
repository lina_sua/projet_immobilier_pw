-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : lun. 10 jan. 2022 à 13:51
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `lina`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_agence`
--

DROP TABLE IF EXISTS `t_agence`;
CREATE TABLE IF NOT EXISTS `t_agence` (
  `nom_agence` varchar(80) NOT NULL,
  `description_agence` varchar(500) NOT NULL,
  `pseudo` varchar(80) NOT NULL,
  PRIMARY KEY (`nom_agence`),
  KEY `ps_ag_FK` (`pseudo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_agence`
--

INSERT INTO `t_agence` (`nom_agence`, `description_agence`, `pseudo`) VALUES
('immotl', 'Roazhon Immobilier, expérience et expertise du marché de l\'Ille-et-Vilaine, à votre disposition pour vos ventes, achats, locations et estimations. Nos collaborateurs mettent à votre service tout notre savoir faire et notre professionnalisme pour que votre projet immobilier se déroule dans les meilleurs conditions. Notre réseau de relations facilite et accélère le processus de vente ou de location de votre bien (appartement, maison, terrain, etc.). ', 'banana');

-- --------------------------------------------------------

--
-- Structure de la table `t_banderole`
--

DROP TABLE IF EXISTS `t_banderole`;
CREATE TABLE IF NOT EXISTS `t_banderole` (
  `id_image` int(11) NOT NULL,
  `id_bienImmobilier` int(11) NOT NULL,
  PRIMARY KEY (`id_image`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_banderole`
--

INSERT INTO `t_banderole` (`id_image`, `id_bienImmobilier`) VALUES
(1, 1),
(5, 2),
(9, 4);

-- --------------------------------------------------------

--
-- Structure de la table `t_bienimmobilier`
--

DROP TABLE IF EXISTS `t_bienimmobilier`;
CREATE TABLE IF NOT EXISTS `t_bienimmobilier` (
  `id_bienImmobilier` int(11) NOT NULL AUTO_INCREMENT,
  `immo_titre` varchar(80) DEFAULT NULL,
  `immo_type` varchar(80) DEFAULT NULL,
  `immo_prix` int(11) DEFAULT NULL,
  `immo_ville` varchar(80) DEFAULT NULL,
  `immo_codePostal` varchar(80) DEFAULT NULL,
  `immo_adresse` varchar(80) DEFAULT NULL,
  `immo_description` varchar(80) DEFAULT NULL,
  `immo_date_ajoute` date DEFAULT NULL,
  `pseudo` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id_bienImmobilier`),
  UNIQUE KEY `immo_titre` (`immo_titre`),
  KEY `pse_bien_FK` (`pseudo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_bienimmobilier`
--

INSERT INTO `t_bienimmobilier` (`id_bienImmobilier`, `immo_titre`, `immo_type`, `immo_prix`, `immo_ville`, `immo_codePostal`, `immo_adresse`, `immo_description`, `immo_date_ajoute`, `pseudo`) VALUES
(1, 'lujos', 'location', 300, 'rennes', '35000', 'rue zacharie mestre', 'un lindo lugar', '2022-01-11', 'banana'),
(2, 'casa nueva', 'vente', 400, 'rennes', '35000', 'rue sabana focus', 'c\'est une maison des années 80', '2021-12-21', 'lina'),
(4, 'ensayo', 'vente', 300000, 'Rennes', '35000', 'rue la paz', 'este es un ensayo', '2022-01-09', 'lina');

-- --------------------------------------------------------

--
-- Structure de la table `t_collaborator`
--

DROP TABLE IF EXISTS `t_collaborator`;
CREATE TABLE IF NOT EXISTS `t_collaborator` (
  `collabo_id` int(11) NOT NULL AUTO_INCREMENT,
  `collabo_nom` varchar(80) DEFAULT NULL,
  `collabo_poste` varchar(80) DEFAULT NULL,
  `collabo_image` varchar(80) NOT NULL,
  `pseudo` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`collabo_id`),
  KEY `colla_pse_FK` (`pseudo`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_collaborator`
--

INSERT INTO `t_collaborator` (`collabo_id`, `collabo_nom`, `collabo_poste`, `collabo_image`, `pseudo`) VALUES
(1, 'Adam Phillips', 'Business Consultant', 'team-1.jpg', 'lina'),
(2, 'Dylan Adams', 'Agent Consultant', 'team-2.jpg', 'lina'),
(3, 'Gloria Edwards', 'Agent Consultant', 'team-3.jpg', 'lina'),
(4, 'Josh Dunn', 'Agent consultant', 'team-4.jpg', 'lina');

-- --------------------------------------------------------

--
-- Structure de la table `t_compte_utilisateur`
--

DROP TABLE IF EXISTS `t_compte_utilisateur`;
CREATE TABLE IF NOT EXISTS `t_compte_utilisateur` (
  `pseudo` varchar(80) NOT NULL,
  `mot_passe` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`pseudo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_compte_utilisateur`
--

INSERT INTO `t_compte_utilisateur` (`pseudo`, `mot_passe`) VALUES
('banana', '2916c7d83bb4167ff03e11cbb78e945e'),
('David', '823f5ce9113bd409dcf9831867cee8ef'),
('lina', '2e3817293fc275dbee74bd71ce6eb056'),
('papa', '777bbb7869ae8193249f8ff7d3e59afe'),
('TristanS', '6c572727b088b74f71f23560fc3bf5c6'),
('zedzd', '90bb1b62c5c04db70643f96dea27aaa0');

-- --------------------------------------------------------

--
-- Structure de la table `t_favoris`
--

DROP TABLE IF EXISTS `t_favoris`;
CREATE TABLE IF NOT EXISTS `t_favoris` (
  `pseudo` varchar(80) NOT NULL,
  `id_bienImmobilier` int(11) NOT NULL,
  UNIQUE KEY `id_bienImmobilier` (`id_bienImmobilier`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_favoris`
--

INSERT INTO `t_favoris` (`pseudo`, `id_bienImmobilier`) VALUES
('David', 1),
('lina', 4);

-- --------------------------------------------------------

--
-- Structure de la table `t_image`
--

DROP TABLE IF EXISTS `t_image`;
CREATE TABLE IF NOT EXISTS `t_image` (
  `id_image` int(11) NOT NULL AUTO_INCREMENT,
  `nom_image` varchar(80) NOT NULL,
  `id_bienImmobilier` int(11) NOT NULL,
  PRIMARY KEY (`id_image`,`id_bienImmobilier`) USING BTREE,
  KEY `bie_img_FK` (`id_bienImmobilier`,`id_image`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_image`
--

INSERT INTO `t_image` (`id_image`, `nom_image`, `id_bienImmobilier`) VALUES
(1, 'casa1.jpeg', 1),
(2, 'casa1.1.jpg', 1),
(3, 'casa1.2.jpg', 1),
(4, 'casa1.3.jpg', 1),
(5, 'casa2.jpg', 2),
(6, 'casa2.1.jpg', 2),
(7, 'casa2.2.jpg', 2),
(8, 'casa2.3.jpg', 2),
(9, 'casa1.1.jpg', 4);

-- --------------------------------------------------------

--
-- Structure de la table `t_partenaire`
--

DROP TABLE IF EXISTS `t_partenaire`;
CREATE TABLE IF NOT EXISTS `t_partenaire` (
  `id_partenaire` int(11) NOT NULL AUTO_INCREMENT,
  `image_partenaire` varchar(80) NOT NULL,
  `nom_partenaire` varchar(80) NOT NULL,
  `lienSite` varchar(100) NOT NULL,
  `nom_agence` varchar(80) NOT NULL,
  PRIMARY KEY (`id_partenaire`),
  KEY `part_age_FK` (`nom_agence`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_profil_utilisateur`
--

DROP TABLE IF EXISTS `t_profil_utilisateur`;
CREATE TABLE IF NOT EXISTS `t_profil_utilisateur` (
  `nom` varchar(80) NOT NULL,
  `prenom` varchar(80) DEFAULT NULL,
  `email` varchar(80) NOT NULL,
  `statut` varchar(4) DEFAULT NULL,
  `date_creation` date DEFAULT NULL,
  `pseudo` varchar(80) NOT NULL,
  PRIMARY KEY (`pseudo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_profil_utilisateur`
--

INSERT INTO `t_profil_utilisateur` (`nom`, `prenom`, `email`, `statut`, `date_creation`, `pseudo`) VALUES
('sua', 'lina', 'banana@gmail.com', 'A', '2020-01-17', 'banana'),
('Torres', 'hernan', 'hernan@hotmail.com', 'C', '2022-01-06', 'David'),
('sua', 'maria', 'maria@hotmail.com', 'A', '2022-01-06', 'lina'),
('paquito', 'gallego', 'pap@gmail.com', 'C', '2022-01-04', 'papa'),
('Le Saux', 'Tristan', 'tristan@hotmail.com', 'C', '2022-01-04', 'TristanS'),
('dfdfd', 'jkjk', 'jklo@gmail.com', 'C', '2022-01-04', 'zedzd');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `t_agence`
--
ALTER TABLE `t_agence`
  ADD CONSTRAINT `ps_ag_FK` FOREIGN KEY (`pseudo`) REFERENCES `t_compte_utilisateur` (`pseudo`);

--
-- Contraintes pour la table `t_banderole`
--
ALTER TABLE `t_banderole`
  ADD CONSTRAINT `img_FK` FOREIGN KEY (`id_image`) REFERENCES `t_image` (`id_image`);

--
-- Contraintes pour la table `t_bienimmobilier`
--
ALTER TABLE `t_bienimmobilier`
  ADD CONSTRAINT `pse_bien_FK` FOREIGN KEY (`pseudo`) REFERENCES `t_compte_utilisateur` (`pseudo`);

--
-- Contraintes pour la table `t_collaborator`
--
ALTER TABLE `t_collaborator`
  ADD CONSTRAINT `colla_pse_FK` FOREIGN KEY (`pseudo`) REFERENCES `t_compte_utilisateur` (`pseudo`);

--
-- Contraintes pour la table `t_favoris`
--
ALTER TABLE `t_favoris`
  ADD CONSTRAINT `fa_bien_FK` FOREIGN KEY (`id_bienImmobilier`) REFERENCES `t_bienimmobilier` (`id_bienImmobilier`);

--
-- Contraintes pour la table `t_image`
--
ALTER TABLE `t_image`
  ADD CONSTRAINT `bie_img_FK` FOREIGN KEY (`id_bienImmobilier`) REFERENCES `t_bienimmobilier` (`id_bienImmobilier`);

--
-- Contraintes pour la table `t_partenaire`
--
ALTER TABLE `t_partenaire`
  ADD CONSTRAINT `part_age_FK` FOREIGN KEY (`nom_agence`) REFERENCES `t_agence` (`nom_agence`);

--
-- Contraintes pour la table `t_profil_utilisateur`
--
ALTER TABLE `t_profil_utilisateur`
  ADD CONSTRAINT `pse_pro_FK` FOREIGN KEY (`pseudo`) REFERENCES `t_compte_utilisateur` (`pseudo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
