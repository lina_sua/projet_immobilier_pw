<!DOCTYPE html>
<?php
/* Vérification ci-dessous à faire sur toutes les pages dont l'accès est
autorisé à un utilisateur connecté. */
session_start();
if(!isset($_SESSION['login'])) //verification pour le pseudo
{
 //Si la session n'est pas ouverte, redirection vers la page du formulaire
header("Location:connection.php");
exit();
}

?>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <title></title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Law Firm Website Template" name="keywords">
        <meta content="Law Firm Website Template" name="description">

        <!-- Favicon -->
        <link href="../img/favicon.ico" rel="icon">

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=EB+Garamond:ital,wght@1,600;1,700;1,800&family=Roboto:wght@400;500&display=swap" rel="stylesheet"> 
        
        <!-- CSS Libraries -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
        <link href="lib/animate/animate.min.css" rel="stylesheet">
        <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

        <!-- Template Stylesheet -->
        <link href="../css/style.css" rel="stylesheet">
    </head>

    <body>
        <div class="wrapper">
            <!-- Top Bar Start -->
            <div class="top-bar">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="logo">
                                
                                    <h1>ESPACE COMPTE</h1>
                               
                            </div>
                        </div>
                       

                        <div class="col-lg-9">
                            <div class="top-bar-right">
                                <div class="text">
                                    <h2></h2>
                                    <p></p>
                                </div>
                                <div class="text">
                                    
					
                                </div>
                                <div class="social">
                                    <a href=""><i class="fab fa-twitter"></i></a>
                                    <a href=""><i class="fab fa-facebook-f"></i></a>
                                    <a href=""><i class="fab fa-linkedin-in"></i></a>
                                    <a href=""><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->
<!-- Nav Bar Start -->
            <div class="nav-bar">
                <div class="container-fluid">
                    <nav class="navbar navbar-expand-lg bg-dark navbar-dark">
                        <a href="#" class="navbar-brand">Menu</a>
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                            <div class="navbar-nav mr-auto">
                                <a href="../index.php" class="nav-item nav-link active">Accueil
& profil(s)</a>
<a href="../control/immoDispo.php" class="nav-item nav-link">biens disponibles</a>
                                <a href="../control/favorites.php" class="nav-item nav-link">aller à favorites</a>
                    
                                <a href="../control/desconexion.php" class="nav-item nav-link">Déconnexion</a>
                            </div>
                            
                        </div>
                    </nav>
                </div>
            </div>
            <!-- Nav Bar End -->
           
 <!-- About Start -->
            <div class="about">
                <div class="container">
                    <div class="row align-items-center">
<div class="col-lg-5 col-md-6">
                            <div class="about-img">
                                <img src="../img/img2.jfif" alt="Image">
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-6">
                            <div class="about-img">
                                <img src="../img/img4.jfif" alt="Image">
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-6">
 				<div class="section-header">
<h2>
<?php                              
echo("bienvenue ".$_SESSION['login']."");
?>
</h2>
                            	</div>
				
                               <p>
                               <?php
					//appelle base des données
					$host = 'localhost';
	$dbname = 'lina';
	$username = 'root';
	$password = '';
	
	$dsn ="mysql:host=$host;dbname=$dbname";
	$sql = "SELECT * FROM t_profil_utilisateur where pseudo = '".$_SESSION['login']."';";
	try{
		$connection = new PDO($dsn, $username, $password);
		$req = $connection->query($sql);
		//Affichage de la requête préparée
			//tous les données de profil de la personne qui vient de ovrir sont compte
						echo ("<br />");
						$profil = $req->fetch(PDO::FETCH_ASSOC);
						echo("nom =".$profil['nom']);echo ("<br />");
						echo("\nprenom = ".$profil['prenom']);echo ("<br />");
						echo("\ne-mail = ".$profil['email']);echo ("<br />");
						echo("\npseudo= ".$profil['pseudo']);echo ("<br />");
						
	}catch(PDOException $e){
		echo ("Erreur entrer base données : ".$e->getMessage());
	}

?>
					<?php
				
			
					if($_SESSION['statut'] ==  'A'){//si il s'agit d'une compte administrateur

					?>
                                </p>
				
                                <div class="section-header">
                                <h2>Bienvenu à l'espace Administrateur</h2>
								
								<br />
								
								<h1>Gestion des
 biens imobilieres</h1>
 <p>
 <form action="admin_biens.php" method="post">
 
 <p> vous ne pouvaiz pas changer les dates de création</p>
										<label for="option">Votre choix à modifier </label><select name="option" id="option">
										<option>Inserter</option>
										<option>Modifier</option>
										<option>Eliminer</option> 		
										</select><br><br>
										
										<p><input type="submit" value="Valider" ></p>
										</form>
					<?php
					}
?>					
								
</p>
								
                            	
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- About End -->

        <!-- JavaScript Libraries -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
        <script src="lib/easing/easing.min.js"></script>
        <script src="lib/owlcarousel/owl.carousel.min.js"></script>
        <script src="lib/isotope/isotope.pkgd.min.js"></script>

        <!-- Template Javascript -->
        <script src="../js/main.js"></script>
    </body>
</html>

