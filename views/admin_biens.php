<?php
session_start();
if(!isset($_SESSION['login'])) //verification pour le pseudo
{
 //Si la session n'est pas ouverte, redirection vers la page du formulaire
header("Location:views/connection.php");
exit();
}
if($_SESSION['statut'] ==  'A'){


?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title></title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Law Firm Website Template" name="keywords">
        <meta content="Law Firm Website Template" name="description">

        <!-- Favicon -->
        <link href="../img/favicon.ico" rel="icon">

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=EB+Garamond:ital,wght@1,600;1,700;1,800&family=Roboto:wght@400;500&display=swap" rel="stylesheet"> 
        
        <!-- CSS Libraries -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
        <link href="lib/animate/animate.min.css" rel="stylesheet">
        <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

        <!-- Template Stylesheet -->
        <link href="../css/style.css" rel="stylesheet">
    </head>

    <body>
	 <!-- Top Bar Start -->
            <div class="top-bar">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="logo">
               
                                    <h1>client profil</h1>
                                    <!-- <img src="img/logo.jpg" alt="Logo"> -->
                                
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="top-bar-right">
                                <div class="text">
                                    <h2>immobilier des projects des étudiantes</h2>
                                    <p>Departement informatiquerennes1</p>

                                </div>
                                <div class="social">
                                    <a href=""><i class="fab fa-twitter"></i></a>
                                    <a href=""><i class="fab fa-facebook-f"></i></a>
                                    <a href=""><i class="fab fa-linkedin-in"></i></a>
                                    <a href=""><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->
        <div class="wrapper">
            
<!-- Nav Bar Start -->
            <div class="nav-bar">
                <div class="container-fluid">
                    <nav class="navbar navbar-expand-lg bg-dark navbar-dark">
                        <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                            <div class="navbar-nav mr-auto">
				<a href="../index.php" class="nav-item nav-link active">home</a>
                                <a href="admin_accueil.php" class="nav-item nav-link active">accueil compte</a>
								<a href="../control/desconnexion.php" class="nav-item nav-link active">desconnexion</a>
			    </div>    
                        </div>
                    </nav>
                </div>
            </div>
            <!-- Nav Bar End -->
	
<!--About Start -->
            <div class="about">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-5 col-md-6">
                            <div class="about-img">
                                <img src="../img/img2.jfif" alt="Image">
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-6">
				<h2>partie compte utilisateur</h2>
					<p> Bonjour,
						<?php	
						//formulaires pour inserter, modifier et effacer 	
								$option = $_POST['option'];
		if($option == 'Inserter'){
		?>
		<p>
		<form action="../control/insererbiens.php"" method="post">
										<fieldset>
											<legend>ajouter les donnée d'un biens :</legend>
											<p> les caise * sont obligatoire </p>
											<p>titre *:
												<input type="text" name="titre" placeholder="titre" maxlength="20" required="required"  />
											</p>
										</fieldset>
										<p>type *: <input type="text" name="type" placeholder="type" maxlength="20" required="required"/></p>
										<p>prix *: <input type="number" name="prix" placeholder="prix" maxlength="20" required="required" /></p>
										<p>ville *: 
											<input type="text" name="ville" placeholder="ville" maxlength="20"  />
										</p>
										<p>codePostal *: 
											<input type="number" name="codePostal" placeholder="codePostal" maxlength="20" required="required" />
										</p>
										<p>adrese *: 
											<input type="text" name="adresse" maxlength="20" required="required" />
										</p>
										<p>description *: 
											<input type="text" name="description" maxlength="500" required="required" />
										</p>
										<p>confirmation choix *: 
											<label for="option">Votre choix à modifier </label><select name="option1" id="option1">
										<option>Inserter</option>
										<option>Modifier</option>
										<option>Eliminer</option> 		
										</select><br><br>
										</p>
																				
										<p><input type="submit" value="Valider" ></p>
								
									</form>
		</p>
	<?php
	}
	if($option == 'Modifier'){
		
	?>
		<p>
		<form action="../control/insererbiens.php"" method="post">
										<fieldset>
											<legend>chosir l'élement à modifier et écrivez le noveau donnée :</legend>
											<p> les caise * sont obligatoire </p>
											<p>titre *:
												<input type="text" name="titre" placeholder="titre" maxlength="20" required="required"  />
											</p>
										</fieldset>
										<p>type *: <input type="text" name="type" placeholder="type" maxlength="20" required="required"/></p>
										<p>prix *: <input type="number" name="prix" placeholder="prix" maxlength="20" required="required" /></p>
										<p>ville *: 
											<input type="text" name="ville" placeholder="ville" maxlength="20"  />
										</p>
										<p>codePostal *: 
											<input type="number" name="codePostal" placeholder="codePostal" maxlength="20" required="required" />
										</p>
										<p>adrese *: 
											<input type="text" name="adresse" maxlength="20" required="required" />
										</p>
										<p>pseudo *: 
											<input type="text" name="description" maxlength="20"  />
										</p>
										<p>confirmation choix *: 
											<label for="option">Votre choix à modifier </label><select name="option1" id="option1">
										<option>Inserter</option>
										<option>Modifier</option>
										<option>Eliminer</option> 		
										</select><br><br>
										</p>
																				
										<p><input type="submit" value="Valider" ></p>
								
									</form>
		</p>
		<?php
	}
	if($option == 'Eliminer'){
		
	?>
	<p>
		<form action="../control/insererbiens.php"" method="post">
										<fieldset>
											<legend>biens à effacer :</legend>
											<p> les caise * sont obligatoire </p>
											<p>titre *:
												<input type="text" name="titre" placeholder="titre" maxlength="20" required="required"  />
											</p>
										</fieldset>
										<p>type *: <input type="text" name="type" placeholder="type" maxlength="20" required="required"/></p>
		<p>confirmation choix *: 
											<label for="option">Votre choix à modifier </label><select name="option1" id="option1">
										<option>Inserter</option>
										<option>Modifier</option>
										<option>Eliminer</option> 		
										</select><br><br>
										</p>
										
										<p><input type="submit" value="Valider" ></p>
								
									</form>
									</p>
				<?php
	}
					?>	
						
								
 
				
					<?php
						
}	
						


?>

</p>			
						
                                </p>
                                <p>
                                </p>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- About End -->
           
 
		
        <!-- JavaScript Libraries -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
        <script src="lib/easing/easing.min.js"></script>
        <script src="lib/owlcarousel/owl.carousel.min.js"></script>
        <script src="lib/isotope/isotope.pkgd.min.js"></script>

        <!-- Template Javascript -->
        <script src="../js/main.js"></script>
    </body>
</html>
