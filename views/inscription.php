

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title></title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Law Firm Website Template" name="keywords">
        <meta content="Law Firm Website Template" name="description">

        <!-- Favicon -->
        <link href="../img/favicon.ico" rel="icon">

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=EB+Garamond:ital,wght@1,600;1,700;1,800&family=Roboto:wght@400;500&display=swap" rel="stylesheet"> 
        
        <!-- CSS Libraries -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
        <link href="lib/animate/animate.min.css" rel="stylesheet">
        <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

        <!-- Template Stylesheet -->
        <link href="../css/style.css" rel="stylesheet">
    </head>

    <body>
        <div class="wrapper">
            <!-- Top Bar Start -->
            <div class="top-bar">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="logo">
                                <a href="../index.html">
                                    <h1>Inscription</h1>
                                    <!-- <img src="img/logo.jpg" alt="Logo"> -->
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="top-bar-right">
                                <div class="text">
                                   <h2>immobilier projet pw</h2>
                                    <p>Departement informatique rennes1</p>
					
                                </div>
                                <div class="social">
                                    <a href=""><i class="fab fa-twitter"></i></a>
                                    <a href=""><i class="fab fa-facebook-f"></i></a>
                                    <a href=""><i class="fab fa-linkedin-in"></i></a>
                                    <a href=""><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	<!-- Top Bar End -->

	<!-- Nav Bar Start -->
            <div class="nav-bar">
                <div class="container-fluid">
                    <nav class="navbar navbar-expand-lg bg-dark navbar-dark">
                        <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                            <div class="navbar-nav mr-auto">
                                <a href="../index.php" class="nav-item nav-link active">retour Home</a>
			    </div>    
                        </div>
                    </nav>
                </div>
            </div>
            <!-- Nav Bar End -->

	<!-- About Start -->
            <div class="about">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-5 col-md-6">
                            <div class="about-img">
                                <img src="../img/im1.jfif" alt="Image">
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-6">
                            <div class="section-header">
                                <h2>Bienvenue</h2>
                            </div>
                            <div class="about-text">
								<div id="contenu">
		<script language="JavaScript">

	function submitForm()
	{ 
		var req = null; 

		document.ajax.dyn.value="Started...";
 
		if (window.XMLHttpRequest)
		{
 			req = new XMLHttpRequest();

		} 
		else if (window.ActiveXObject) 
		{
			try {
				req = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e)
			{
				try {
					req = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) {}
			}
        	}


		req.onreadystatechange = function()
		{ 
			document.ajax.dyn.value="Wait server...";
			if(req.readyState == 4)
			{
				if(req.status == 200)
				{
					document.ajax.dyn.value=req.responseText;	
				}	
				else	
				{
					document.ajax.dyn.value="Error: returned status code " + req.status + " " + req.statusText;
				}	
			} 
		}; 
		req.open("GET", "data.txt", true); 
		req.send(null); 
	} 
	</script>
									<form action="../control/action.php" method="post">
										<fieldset>
											<legend>Données personnelles :</legend>
											<p> les caise * sont obligatoire </p>
											<p>Pseudo *:
												<input type="text" name="pseudo" placeholder="pseudo" maxlength="20" required="required"  />
											</p>
										</fieldset>
										<p>Mdp *: <input type="password" name="mdp" placeholder="mot de passe" maxlength="20" required="required"/></p>
										<p>confirmation Mdp *: <input type="password" name="confmdp" placeholder="mot de passe" maxlength="20" required="required" /></p>
										<p>Nom *: 
											<input type="text" name="nom" placeholder="nom" maxlength="20"  />
										</p>
										<p>Prenom *: 
											<input type="text" name="prenom" placeholder="prenom" maxlength="20" required="required" />
										</p>
										<p>adresse e-mail *: 
											<input type="text" name="email" maxlength="20" required="required" />
										</p>
										
										
										<p><input type="submit" value="Valider" ONCLICK="submitForm()"></p>
								
									</form>
			
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- About End -->
			
			<!-- Footer Start -->
            <div class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-lg-4">
                            <div class="footer-about">
                                <h2>About Us</h2>
								
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-8">
                            <div class="row">
                        <div class="col-md-6 col-lg-4">
                            
                        </div>
                        <div class="col-md-6 col-lg-4">
                            
                        </div>
                        <div class="col-md-6 col-lg-4">
                            
                                    <a href=""><i class="fab fa-twitter"></i></a>
                                    <a href=""><i class="fab fa-facebook-f"></i></a>
                                    <a href=""><i class="fab fa-youtube"></i></a>
                                    <a href=""><i class="fab fa-instagram"></i></a>
                                    <a href=""><i class="fab fa-linkedin-in"></i></a>
                               
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                
                
            </div>
            <!-- Footer End -->
           

        <!-- JavaScript Libraries -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
        <script src="lib/easing/easing.min.js"></script>
        <script src="lib/owlcarousel/owl.carousel.min.js"></script>
        <script src="lib/isotope/isotope.pkgd.min.js"></script>

        <!-- Template Javascript -->
        <script src="../js/main.js"></script>
    </body>
</html>
