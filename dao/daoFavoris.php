<?php
require_once("../util/config.php");
class DAOFavoris{
  private $favoris;
  private $connexion;
  
  public function __construct($b) {
    $this->favoris = $b;
	$this->connexion = null;
  }
  
  public function connect(){
	  
	try{
		$this->connexion = new PDO("mysql:host=" . PDO_HOST . ";"."dbname=" . PDO_DBBASE, PDO_USER, PDO_PW);
	}catch (PDOException $e){
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}  	  
  }

  public function getFavoris() {
     return $this->favoris;
  }
  
  public function setFavoris($p) {
       $this->favoris = $p;
  }
  //version simplifiée d'une dao avec une méthode add et des parametres nommés et bindValue
  public function add() {
      
	try{
		$this->connect();
		$query = "INSERT INTO t_favoris (pseudo, id_bienImmobilier) VALUES(:n, :p);";
		$data = array( 
		':n'=> $this->favoris->getPseudo(),
		':p'=> $this->favoris->getBienImmobilier()		
		);
		
		$sth = $this->connexion->prepare( $query );
		$res=$sth->execute( $data );
		$this->connexion = null;
		return $res;
	}catch (PDOException $e){
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}
  }
  
   public function delete1() {
      
	try{
		$this->connect();
		$query = " delete from t_favoris where pseudo= :p and id_bienImmobilier= :s "; 
		$data = array( 
		':p'=>$this->favoris->getPseudo(),
		':s'=>$this->favoris->getBienImmobilier()	
		);
		$sth = $this->connexion->prepare( $query );
		$res=$sth->execute( $data );
		$this->connexion = null;
		
		return $res;
	}catch (PDOException $e){
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}
  }
  
   
}


?>