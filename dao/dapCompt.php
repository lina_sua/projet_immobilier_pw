<?php
require_once("../util/config.php");
class DAOCompte{
  private $compte;
  private $connexion;
  
  public function __construct($c) {
    $this->compte = $c;
	$this->connexion = null;
  }
  
  public function connect(){
	  
	try{
		$this->connexion = new PDO("mysql:host=" . PDO_HOST . ";"."dbname=" . PDO_DBBASE, PDO_USER, PDO_PW);
	}catch (PDOException $e){
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}  	  
  }

  public function getCompte() {
     return $this->compte;
  }
  
  public function setCompte($c) {
       $this->compte = $c;
  }
  //version simplifiée d'une dao avec une méthode add et des parametres nommés et bindValue
  public function add() {
      
	try{
		$this->connect();
		$query = " INSERT INTO t_compte_utilisateur values(:pseudo,MD5(:mdp))"; 
		$data = array( 
		':pseudo'=>$this->compte->getPseudo(),
		':mdp'=> $this->compte->getMdp()
		);
		$sth = $this->connexion->prepare( $query );
		$res=$sth->execute( $data );
		$this->connexion = null;
		return $res;
	}catch (PDOException $e){
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}
  }
  
   public function delete() {
      
	try{
		$this->connect();
		$query = " delete from t_compte_utilisateur where pseudo=:p "; 
		$data = array( 
		':p'=>$this->compte->getPseudo()
		);
		$sth = $this->connexion->prepare( $query );
		$res=$sth->execute( $data );
		$this->connexion = null;
		return $res;
	}catch (PDOException $e){
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}
  }
  
     public function update() {
      
	try{
		$this->connect();
		$query = " update compte_utilisateur set pseudo=:p, mot_passe=:m "; 
		$data = array( 
		':p'=>$this->compte->getPseudo(),
		':m'=> $this->compte->getMdp(), 
		);
		$sth = $this->connexion->prepare( $query );
		$res=$sth->execute( $data );
		$this->connexion = null;
		return $res;
	}catch (PDOException $e){
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}
  }
   
}


?>