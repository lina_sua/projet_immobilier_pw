<?php
require_once("util/config.php");
class DAOCollaborator{
  private $collabo;
  private $connexion;
  
  public function __construct($b) {
    $this->biens = $b;
	$this->connexion = null;
  }
  
  public function connect(){
	  
	try{
		$this->connexion = new PDO("mysql:host=" . PDO_HOST . ";"."dbname=" . PDO_DBBASE, PDO_USER, PDO_PW);
	}catch (PDOException $e){
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}  	  
  }

  public function getCollaborator() {
     return $this->collabo;
  }
  
  public function setBiens($p) {
       $this->collabo = $p;
  }
  
  public function affiche() {
      
	try{
		$this->connect();
		$query = "SELECT * FROM t_collaborator ;";
		$sth = $this->connexion->prepare( $query );
        while ($immo = $sth->fetch(PDO::FETCH_ASSOC))
        {	
        
            $this->collabo->setNom = $immo['collabo_nom'] ;
            $this->collabo->setPoste = $immo['collabo_poste']  ;
            $this->collabo->setImage =$immo['collabo_image']  ;
    
        }

		$this->connexion = null;

	
	}catch (PDOException $e){
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}
  }
   
}


?>