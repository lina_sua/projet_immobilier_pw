<?php
require_once("../util/config.php");
class DAOProfil{
  private $profil;
  private $connexion;
  
  public function __construct($p) {
    $this->profil = $p;
	$this->connexion = null;
  }
  
  public function connect(){
	  
	try{
		$this->connexion = new PDO("mysql:host=" . PDO_HOST . ";"."dbname=" . PDO_DBBASE, PDO_USER, PDO_PW);
	}catch (PDOException $e){
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}  	  
  }

  public function getProfil() {
     return $this->profil;
  }
  
  public function setProfil($p) {
       $this->profil = $p;
  }
  //version simplifiée d'une dao avec une méthode add et des parametres nommés et bindValue
  public function add() {
      
	try{
		$this->connect();
		$query = " INSERT INTO t_profil_utilisateur values(:n,:p,:e,'C',CURDATE(),:ps)"; 
		$data = array( 
		':n'=>$this->profil->getNom(),
		':p'=> $this->profil->getPrenom(),
		':e'=> $this->profil->getEmail(),
		':ps'=> $this->profil->getPseudo()
		);
		$sth = $this->connexion->prepare( $query );
		$res=$sth->execute( $data );
		$this->connexion = null;
		return $res;
	}catch (PDOException $e){
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}
  }
  
   public function delete() {
      
	try{
		$this->connect();
		$query = " delete from t_profil_utilisateur where pseudo=:p "; 
		$data = array( 
		':p'=>$this->profil->getPseudo()
		);
		$sth = $this->connexion->prepare( $query );
		$res=$sth->execute( $data );
		$this->connexion = null;
		return $res;
	}catch (PDOException $e){
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}
  }
  
     public function update() {
      
	try{
		$this->connect();
		$query = " update t_profil_utilisateur set nom=:n, prenom=:pre, email =:e, statut =:s where pseudo =:p "; 
		$data = array( 
		':n'=>$this->profil->getNom(),
		':pre'=> $this->profil->getPrenom(), 
		':e'=>$this->compte->getEmail(),
		':s'=> $this->compte->getStatut(), 
		':p'=>$this->compte->getPseudo()
		);
		$sth = $this->connexion->prepare( $query );
		$res=$sth->execute( $data );
		$this->connexion = null;
		return $res;
	}catch (PDOException $e){
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}
  }
   
}


?>