<?php
require_once("../util/config.php");
class DAOBiens{
  private $biens;
  private $connexion;
  
  public function __construct($b) {
    $this->biens = $b;
	$this->connexion = null;
  }
  
  public function connect(){
	  
	try{
		$this->connexion = new PDO("mysql:host=" . PDO_HOST . ";"."dbname=" . PDO_DBBASE, PDO_USER, PDO_PW);
	}catch (PDOException $e){
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}  	  
  }

  public function getBiens() {
     return $this->biens;
  }
  
  public function setBiens($p) {
       $this->biens = $p;
  }
  //version simplifiée d'une dao avec une méthode add et des parametres nommés et bindValue
  public function add() {
      
	try{
		$this->connect();
		$query = "INSERT INTO t_bienimmobilier (immo_titre, immo_type,immo_prix,immo_ville,immo_codePostal,immo_adresse,immo_description,immo_date_ajoute, pseudo) VALUES(:n, :p, :e, :v, :c, :a, :d,CURDATE(), :ps);";
		$data = array( 
		':n'=>$this->biens->getTitre(),
		':p'=> $this->biens->getType1(),
		':e'=> $this->biens->getPrix(),
		':v'=> $this->biens->getVille(),
		':c'=> $this->biens->getCodePostal(),
		':a'=> $this->biens->getAdresse(),
		':d'=> $this->biens->getDescription(),
		':ps'=> $this->biens->getPseudo()
		);
		
		$sth = $this->connexion->prepare( $query );
		$res=$sth->execute( $data );
		$this->connexion = null;
		return $res;
	}catch (PDOException $e){
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}
  }
  
   public function delete1($t,$ti) {
      
	try{
		$this->connect();
		$query = " delete from t_bienimmobilier where immo_titre= :p and immo_type= :s "; 
		$data = array( 
		':p'=>$t,
		':s'=>$ti
		);
		$sth = $this->connexion->prepare( $query );
		$res=$sth->execute( $data );
		$this->connexion = null;
		
		return $res;
	}catch (PDOException $e){
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}
  }
  
     public function update() {
      
	try{
		$this->connect();
		$query = " update t_bienimobilier set immo_type=:pre, immo_prix =:e, immo_ville =:s ,immo_codePostal= :i,immo_adresse = :j, where pseudo =:p and immo_titre=:n "; 
		$data = array( 
		':n'=>$this->biens->getTitre(),
		':pre'=> $this->biens->getType1(),
		':e'=> $this->biens->getPrix(),
		':s'=> $this->biens->getVille(),
		':i'=> $this->biens->getCodePostal(),
		':j'=> $this->biens->getAdresse(),
		':ps'=> $this->biens->getPseudo()
		
		);
		$sth = $this->connexion->prepare( $query );
		$res=$sth->execute( $data );
		$this->connexion = null;
		return $res;
	}catch (PDOException $e){
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}
  }
   
}


?>